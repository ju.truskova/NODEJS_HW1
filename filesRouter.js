const express = require('express');

const router = express.Router();
const { createFile, getFiles, getFile, deleteFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile);

router.delete('/:filename', deleteFile);

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router
};

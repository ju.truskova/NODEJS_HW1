const fs = require('fs');
const path = require('path');

const DIR = 'files';

// constants...

function createFile (req, res, next) {
  if(!req.body.filename || !req.body.content){
    return res.status(400).send({"message": "Add params filename and content"})
  }

  const extension = path.parse(req.body.filename).ext;
  const extensions = [ '.log', '.txt', '.json', '.yaml', '.xml', '.js'];

  if (!extensions.includes(extension)){
    return res.status(400).send({"message": "File type is not supported"})
  };

  fs.writeFileSync(path.join(__dirname, DIR, req.body.filename), req.body.content);

  res.status(200).send({ "message": "File created successfully" });
}

function getFiles (req, res, next) {
  const files = fs.readdirSync((path.join(__dirname, DIR)));

  res.status(200).send({
    "message": "Success",
     files});
}

const getFile = (req, res, next) => {
  const filePath = path.join(__dirname, DIR, req.params.filename);

  if (!fs.existsSync(filePath)){
    return res.status(400).send({"message": "File is not found"})
  }

  const content = fs.readFileSync(filePath, { encoding: 'utf8' });
  const fileInfo = fs.statSync(filePath);

  res.status(200).send({
    "message": "Success",
    "filename": req.params.filename,
    content,
    "extension": path.parse(req.params.filename).ext.split(".")[1],
    "uploadedDate": fileInfo.birthtime});
}

const deleteFile = (req, res, next) => {
  const filePath = path.join(__dirname, DIR, req.params.filename);
  
  if (!fs.existsSync(filePath)){
    return res.status(400).send({"message": "File is not found"})
  }

  fs.unlink( filePath, () => {
    res.status(200).send({ "message": "File deleted successfully" });
  })
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  deleteFile
}
